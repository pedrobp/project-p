﻿using UnityEngine;
using System.Collections;

public class WaypointController : MonoBehaviour {
	
	public GameObject box;
	private GameObject[] boxSpawn;
	private int spawn;

	//Find Spawn points
	void Start () {
		boxSpawn = GameObject.FindGameObjectsWithTag("WandarPoint") as GameObject[];
	}

	//Trigger if player colides
	void OnTriggerEnter(Collider obj){
		//obj = GameObject.FindGameObjectWithTag ("Player") as GameObject;
		if (obj.gameObject.tag == "Player") {
			Invoke ("SpawnWaypoint", 0);
			GlobalSettings.Score = GlobalSettings.Score+200;
		}
	}

	//Spawn Next waypoint
	void SpawnWaypoint(){
		spawn = Random.Range (0, boxSpawn.Length);
			Destroy(gameObject, 0.1f);
			GameObject way = Instantiate (box, boxSpawn [spawn].transform.position, Quaternion.identity) as GameObject;
	}
}
