﻿using UnityEngine;
using System.Collections;

public class PatrolState : FSMState
{
	public PatrolState(Transform[] wp) 
	{ 
		Debug.Log ("Entered patrol state");
		waypoints = wp;
		stateID = FSMStateID.Patrolling;

		//Finds destination Point
		int random = Random.Range(0, waypoints.Length);
		destPos = waypoints [random].position;
	}
	
	public override void Reason(Transform player, Transform npc)
	{
		//Check the distance with player
		//When the distance is near, transition to chase state
		if (Vector3.Distance(npc.position, player.position) <= 35.0f)
		{
			Debug.Log("Switch to Chase State");
			npc.GetComponent<NPCController>().SetTransition(Transition.SawPlayer);
		}
	}
	 
	public override void Act(Transform player, Transform npc)
	{
		//Find another random patrol point if the current point is reached
		if (Vector3.Distance(npc.position, destPos) <= 2.0f)
		{
			Debug.Log("Reached to the destination point\ncalculating the next point");

			//Find Next Point
			int random = Random.Range(0, waypoints.Length);
			destPos = waypoints[random].position;
		}

		//Move to destination
		FSM fsm = npc.GetComponent<FSM>();

		fsm.agent.SetDestination(destPos);
	}
}