﻿using UnityEngine;
using System.Collections;

public class ChaseState : FSMState
{
	public ChaseState(Transform[] wp) 
	{ 
		waypoints = wp;
		stateID = FSMStateID.Chasing;

	}
	
	public override void Reason(Transform player, Transform npc)
	{	
		//Check the distance from player
		//If reached player End Game
		float dist = Vector3.Distance(npc.position, destPos);
		if (dist <= 1.0f)
		{
			Application.LoadLevel(2);
		}
		//Go back to patrol is it become too far
		else
		if (dist >= 40.0f)
		{
			Debug.Log("Switch to Patrol state");
			npc.GetComponent<NPCController>().SetTransition(Transition.LostPlayer);
		}
	}
	
	public override void Act(Transform player, Transform npc)
	{
		//Set Player as destination position
		destPos = player.position;

		//Move to Player
		FSM fsm = npc.GetComponent<FSM>();

		fsm.agent.SetDestination(destPos);

	}
}
