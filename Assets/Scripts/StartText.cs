﻿using UnityEngine;
using System.Collections;

public class StartText : MonoBehaviour {

	public Renderer rend;

	void Start(){
		rend = GetComponent<Renderer>();
		rend.enabled = true;
		rend.material.color = Color.green;
	}
	
	void OnMouseEnter(){
		rend.material.color = Color.white;
	}
	
	void OnMouseExit() {
		rend.material.color = Color.green;
	}
}
