﻿using UnityEngine;
using System.Collections;

public class NPCSpawner : MonoBehaviour {
	
	public GameObject NPC;
	private GameObject spawnPoint;

	// Finds WandarPoints and invokes spawn method
	void Start () {
		spawnPoint = GameObject.FindGameObjectWithTag("Spawn") as GameObject;
		InvokeRepeating ("SpawnNPC", 1.0f, 10.0f);
	}
	
	// Spawns enemies at WandarPoints
	void SpawnNPC () {
		GameObject obj = Instantiate (NPC, spawnPoint.transform.position, Quaternion.identity) as GameObject;
	}
}
