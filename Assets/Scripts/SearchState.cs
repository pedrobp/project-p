﻿using UnityEngine;
using System.Collections;

public class SearchState : FSMState {
	
	//Variable to store the last known position of Player
	private Vector3 destination;

	public SearchState(Transform[] wp) 
	{
		waypoints = wp;
		stateID = FSMStateID.Searching;

	}

	void Start(){
		//Set Destination
		destPos = GameObject.FindGameObjectWithTag ("Player").transform.position;
	}

	public override void Reason(Transform player, Transform npc)
	{
		//Check the distance with player
		//When the distance is near, transition to chase state
		if (Vector3.Distance(npc.position, player.position) <= 35.0f)
		{
			Debug.Log("Switch to Chase State");
			npc.GetComponent<NPCController>().SetTransition(Transition.SawPlayer);
		}

		//When destination reached, switch to patrol state
		if (Vector3.Distance(npc.position, destPos)<=1.0f)
		{
			npc.GetComponent<NPCController>().SetTransition(Transition.PlayerNotFound);
		}
	}
	 
	public override void Act(Transform player, Transform npc)
	{
		//Move to destination
		FSM fsm = npc.GetComponent<FSM>();

		fsm.agent.SetDestination(destPos);
	}
}
