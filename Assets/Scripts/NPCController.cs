﻿using UnityEngine;
using System.Collections;

public class NPCController : AdvancedFSM 
{	
	//Initialize the Finite state machine for the NPC
	protected override void Initialize()
	{
		
		//Get the target enemy(Player)
		GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
		playerTransform = objPlayer.transform;
		
		if (!playerTransform)
			print("Player doesn't exist.. Please add one with Tag named 'Player'");

		//Start Doing the Finite State Machine
		ConstructFSM();
	}
	
	protected override void FSMFixedUpdate()
	{
		CurrentState.Reason(playerTransform, transform);
		CurrentState.Act(playerTransform, transform);
	}
	
	public void SetTransition(Transition t) 
	{ 
		PerformTransition(t); 
	}
	
	private void ConstructFSM()
	{
		//Get the list of points
		pointList = GameObject.FindGameObjectsWithTag("WandarPoint");
		Transform[] waypoints = new Transform[pointList.Length];
		int i = 0;
		foreach(GameObject obj in pointList)
		{
			waypoints[i] = obj.transform;
			i++;
		}

		PatrolState patrol = new PatrolState(waypoints);
		patrol.AddTransition(Transition.SawPlayer, FSMStateID.Chasing);
		
		ChaseState chase = new ChaseState(waypoints);
		chase.AddTransition(Transition.LostPlayer, FSMStateID.Searching);

		SearchState search = new SearchState (waypoints);
		search.AddTransition (Transition.SawPlayer, FSMStateID.Chasing);
		search.AddTransition (Transition.PlayerNotFound, FSMStateID.Patrolling);
		
		AddFSMState(patrol);
		AddFSMState(chase);
		AddFSMState (search);
	}
}
