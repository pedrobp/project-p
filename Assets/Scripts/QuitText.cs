﻿using UnityEngine;
using System.Collections;

public class QuitText : MonoBehaviour {

	public Renderer rend;
	
	void Start(){
		rend = GetComponent<Renderer>();
		rend.enabled = true;
		rend.material.color = Color.yellow;
	}
	
	void OnMouseEnter(){
		rend.material.color = Color.white;
	}
	
	void OnMouseExit() {
		rend.material.color = Color.yellow;
	}
}
