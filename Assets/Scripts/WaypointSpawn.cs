﻿using UnityEngine;
using System.Collections;

public class WaypointSpawn : MonoBehaviour {

	public GameObject wayPoint;
	private GameObject[] wayPointSpawn;

	void Start () {
		//Finds Spawn location
		wayPointSpawn = GameObject.FindGameObjectsWithTag("WandarPoint") as GameObject[];

		//Spawns First Waypoint
		GameObject obj = Instantiate (wayPoint, wayPointSpawn [3].transform.position, Quaternion.identity) as GameObject;
	}
}
