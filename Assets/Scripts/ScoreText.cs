﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour {

	public TextMesh text;

	void Start () {
		//score.text = "Final Score: " + GlobalSettings.Score.ToString ();
		text = (TextMesh)gameObject.GetComponent(typeof(TextMesh));
		text.text = "Final Score: "+GlobalSettings.Score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
